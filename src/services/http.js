/**
 * Created by wesleyguirra on 5/26/17.
 */
import axios from 'axios'

const client = axios.create({baseURL: 'http://localhost:4567/api/v1', headers: {'Authorization': localStorage.getItem('token')}})

axios.interceptors.response.use((response) => {
  return response
}, function (error) {
  if (error.response.status === 401) {
    console.log('unauthorized, logging out ...')
  }
  return Promise.reject(error)
})

export default client
