/**
 * Created by wesleyguirra on 5/30/17.
 */
import http from './http'

export function getMedics () {
  return http.get('medic')
    .then(response => {
      return response.data
    })
}

export function createMedic (payload) {
  return http.post('medic', payload, {headers: {usertype: payload.usertype}})
}
