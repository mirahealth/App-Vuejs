/**
 * Created by wesleyguirra on 5/28/17.
 */
import axios from 'axios'

axios.defaults.baseURL = 'http://localhost:4567/api/v1'

export default {
  login ({commit}, credentials) {
    return axios.post('login', {
      cnpj: credentials.cnpj,
      cps: credentials.cps,
      password: credentials.password
    }, {headers: {usertype: credentials.usertype}})
      .then(response => {
        commit('LOGIN')
        localStorage.setItem('token', response.data.token)
        commit('LOGIN_SUCCESS')
        return response
      })
  },
  register ({commit}, credentials) {
    return axios.post('clinic', {
      name: credentials.name,
      cnpj: credentials.cnpj,
      email: credentials.email,
      password: credentials.password
    }, {headers: {usertype: 'clinica'}})
      .then(response => {
        commit('LOGIN')
        localStorage.setItem('token', response.data.token)
        commit('LOGIN_SUCCESS')
        return response
      })
  },
  logout ({commit}) {
    localStorage.removeItem('token')
    commit('LOGOUT')
  }
}
