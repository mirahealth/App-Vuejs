/**
 * Created by wesleyguirra on 5/28/17.
 */
export default {
  isLoggedIn: !!localStorage.getItem('token')
}
