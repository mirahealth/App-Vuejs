/**
 * Created by wesleyguirra on 5/29/17.
 */
import actions from './actions'
import mutations from './mutations'
import state from './state'

export default {
  actions,
  mutations,
  state
}
