/**
 * Created by wesleyguirra on 5/30/17.
 */
export default {
  'CHANGE_MEDIC' (state, payload) {
    state.medic = payload
  },

  'GET_MEDICS' (state, {list}) {
    state.medics = list
  },
  'CREATE_MEDIC' (state, {medic}) {
    state.medics.push(medic)
  },
  'GET_MEDIC_BY_ID' (state, {medic}) {}
}
