/**
 * Created by wesleyguirra on 5/30/17.
 */
import axios from 'axios'
export default {
  changeMedic (context, payload) {
    context.commit('CHANGE_MEDIC', payload)
  },
  getMedics ({commit}) {
    axios.get('/medic').then(function (response) {
      commit('GET_MEDICS', {list: response.data})
    }).error(function (err) {
      console.log(err)
    })
  },
  getMedicById (commit) {
    axios.get('GET_MEDIC_BY_ID')
  },
  registerMedic (commit) {
    axios.post('/medic').then(function (response) {
      commit('ADD_MEDIC', {medic: response.data})
    }).error(function (err) {
      console.log(err)
    })
  }
}
