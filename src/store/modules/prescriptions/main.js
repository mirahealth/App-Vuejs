/**
 * Created by wesleyguirra on 5/28/17.
 */
import mutations from './mutations'
import state from './state'

export default {
  mutations,
  state
}
