/**
 * Created by wesleyguirra on 5/28/17.
 */
export default {
  'LOGIN' (state) {
    state.pending = true
  },

  'LOGIN_SUCCESS' (state) {
    state.isLoggedIn = true
    state.pending = false
  },
  'LOGOUT' (state) {
    state.isLoggedIn = false
  }
}
