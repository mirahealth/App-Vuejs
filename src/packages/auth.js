/**
 * Created by wesleyguirra on 5/22/17.
 */

export default function (Vue) {
  Vue.auth = {
    isAuthenticated () {
      if (this.getToken()) {
        return true
      } else {
        return false
      }
    },

    getToken () {
      return localStorage.getItem('token')
    },

    setToken (value) {
      return localStorage.setItem('token', value)
    },

    destroyToken () {
      return localStorage.removeItem('token')
    }
  }
  Object.defineProperties(Vue.prototype, {
    $auth: {
      get () {
        return Vue.auth
      }
    }
  })
}

//   login (context, credentials) {
//     axios.post('login', {
//       cnpj: credentials.cnpj,
//       cps: credentials.cps,
//       password: credentials.password
//     }, {
//       headers: { usertype: credentials.usertype }
//     })
//       .then(function (response) {
//         if (response.status === 200) {
//           setToken(response.data)
//           this.authenticated = true
//         }
//       })
//       .catch(function (error) {
//         if (error.status === 400) {
//           console.log(error)
//         }
//       })
//   },
//
//   logout () {
//     destroyToken()
//   },
//
//   signup (context, creds, redir) {
//     axios.post(SIGNUP_URL, {
//       name: creds.name,
//       email: creds.email,
//       cnpj: creds.cnpj,
//       password: creds.password,
//       usertype: creds.usertype
//     })
//       .then(function (response) {
//         setToken(response.data)
//       })
//       .catch(function (error) {
//         console.log(error.response.status)
//       })
//   }
//
// }
