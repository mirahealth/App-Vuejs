import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Panel from '@/components/Panel'
import MedicList from '@/components/modules/list/Medic'
import CreateUser from '@/components/modules/register/CreateUser'
import Prescriptions from '@/components/Prescriptions'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello,
      meta: {isVisitor: true}
    },
    {
      path: '/sign-in',
      name: 'Login',
      component: Login,
      meta: {isVisitor: true}
    },
    {
      path: '/sign-up',
      name: 'Register',
      component: Register,
      meta: {isVisitor: true}
    },
    {
      path: '/panel/',
      name: 'Panel',
      component: Panel,
      meta: {requiresAuth: true},
      children: [
        { path: 'prescriptions', component: Prescriptions },
        { path: 'medics', component: MedicList },
        { path: 'create', component: CreateUser }
      ]
    }
  ]
})

export default router
